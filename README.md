# PlaniFI #

PlaniFI es un planificador para estudiantes de la Facultad de Ingeniería, UBA.
Entre otras cosas, permite obtener un resumen del estado de la carrera (canti-
dad de materias aprobadas, créditos acumulados, promedio, etc.), como también
obtener cierta información acerca de las materias (por ejemplo, el árbol de
correlativas de cierta materia).

Leé el archivo INSTALL para obtener instrucciones de instalación.