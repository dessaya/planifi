# vim: ts=4
#=============================================================================
#
# << PlaniFI - M�dulo con las funciones principales >>
# 
# Diego Essaya <dessaya@fi.uba.ar>
#
# Documentaci�n => PlaniFI.txt
#
#=============================================================================
#
#                            GPL License
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#=============================================================================

package PlaniFI;

#use strict;
#use warnings;

use Exporter();

our $APPNAME = "PlaniFI";
our $VERSION = "1.1";

our @ISA = qw(Exporter);
our @EXPORT = qw(%materias %carrera %personal 
		consulta correlativas habilita guardar_datos_personales
		carreras_disponibles cambiar_estado_materia);
our @EXPORT_OK = qw(init reinit);

# timegm():
use Time::Local;

# Archivos por defecto:
use constant DEF_PERSFILE	=> "personal.dat";
use constant DEF_MATFILE	=> "materias.dat";
use constant DEF_CARRFILE 	=> "informatica.dat";
use constant DEF_CACHEFILE 	=> ".planifi.cache";


#==============================================================================
# Hashes exportados:

our %carrera;
our %materias;
our %personal;

# Archivos:
our %opciones = (
	persfile	=> DEF_PERSFILE,
	matfile		=> DEF_MATFILE,
	carrfile 	=> DEF_CARRFILE,
	cachefile 	=> DEF_CACHEFILE,
);
my ($fh_carrera, $fh_cache, $fh_materias, $fh_personal);


# init_hashes()
#
# Inicializa los hashes %carrera, %materias y %personal con los valores 
# iniciales.
sub init_hashes
{
	%carrera = (
		tipos		=> {
						OBL => "obligatoria",
						OPT => "optativa",
					   },
		estados		=> {
						N	=> "no disponible para cursar",
						D	=> "disponible para cursar",
						A 	=> "aprobada",
						C	=> "cursando",
			           },
		areas		=> { },	# Las areas est�n definidas en el archivo de la
							# carrera.
	);


	%materias = ( );

	%personal = (
		cred_apr	=> 0,		# creditos acumulados
		cred_cur	=> 0,		# creditos cursando
		cred_area	=> 0,		# creditos acumulados en el area
		mat_apr		=> 0,		# cantidad de materias aprobadas
		mat_cur		=> 0,		# cantidad de materias cursando
		promedio	=> 0,		# promedio
	);
}


# get_line(ARCHIVO)
#
# Lee la sig. l�nea de texto, ignorando comentarios y l�neas vac�as.
sub get_line
{
	my $fh = shift;

	while (<$fh>) {
		chomp;					# Eliminar \n
		s/\s*(#.*)?$//;			# Ignorar comentarios y espacios al final.
		next if (/^$/);			# Ignorar l�neas vac�as.

		return $_;
	}

	return 0;
}


# carreras_disponibles()
#
# Devuelve una referencia a un hash con informaci�n acerca de las carreras
# disponibles. El hash devuelto tiene la siguiente estructura:
#  {
# 	informatica.dat => {
# 						nombre => "Ing. en Inform�tica",
# 						areas  => { 
# 									SIDI => "Sistemas Distribuidos",
# 									SIPR => "Sistemas de Producci�n", 
# 								  },
# 						tiene_tesis => 1,
# 					   },
# 	agrimensura.dat => {
# 						nombre => "Agrimensura",
#						areas => {},
#						tiene_tesis => 0,
# 					   },
#  }
sub carreras_disponibles
{
	my $carreras = {};
	my $fh;

	ARCHIVO: for my $fn (glob("*.dat")) {
		if ($fn !~ /$opciones{matfile}|$opciones{persfile}|
					DEF_PERSFILE|DEF_MATFILE/)
		{
			open ($fh, "< ".$fn) or do { warn $fn . ": $!"; next ARCHIVO; };

			while (&get_line($fh)) {
				if (/^\s*carrera\s*=\s*(\S.*)$/) { 
					$carreras->{$fn}{nombre} = $1;
				}
				elsif (/^\[area\s+(\w+)\s+([^\]]+)]$/) { 
					$carreras->{$fn}{areas}{$1} = $2;
				}
				elsif (/^\s*tiene_tesis\s*=\s*(\d)$/) {
					$carreras->{$fn}{tiene_tesis} = $1;
				}
			}

			close $fh or die $fn . ": $!";
		}
	}

	return $carreras;
}

# cache_desactualizado()
#
# devuelve TRUE si no existe el archivo de cache, o si los .dat son
# m�s nuevos que el cache.
sub cache_desactualizado
{
	return 1 if ! -e $opciones{cachefile};
	return 1 if (stat $opciones{matfile})[9] > (stat $opciones{cachefile})[9];
	return 1 if (stat $opciones{persfile})[9] > (stat $opciones{cachefile})[9];
	return 1 if (stat $opciones{carrfile})[9] > (stat $opciones{cachefile})[9];
	return 0;
}


# buscar_materia("COD", \*MATERIAS)
#
# Busca la materia en el archivo, y devuelve ("nombre", cred)
sub buscar_materia
{
	my ($cod, $fh) = @_;
	my $start_cod = 0;

	LOOP: while (1) {
		if (eof $fh) { seek $fh, 0, 0 or die("$!") }
		$_ = &get_line($fh);
		if (/^(\d\d\.\d\d)\s+([^\t]+)\t+(\d+)/) {
			if (!$start_cod) { $start_cod = $1 }
			elsif ($1 eq $start_cod) { last LOOP }

			if ($1 eq $cod) { return ($2, $3) }
		}
	}

	die "La materia $cod no est� definida en el archivo de materias"
}


# agregar_materia("cod", "corr_list", "tipo", "area")
#
# Agrega la materia al hash, parseando la lista de correlativas.
# Adem�s, en caso de estar generando el cache, agrega la l�nea de la materia
# al mismo.
sub agregar_materia
{
	my ($cod, $corr, $tipo, $area) = @_;
	# La busco en el archivo de materias:
	my ($nombre, $cred) = &buscar_materia($1, $fh_materias);

	if (exists $materias{$cod}) { # Materia repetida...
		# Me quedo con la definici�n del �rea elegida.
		if (exists $personal{area} and $personal{area} eq $area)
		{
			$materias{$cod}{tipo} = $tipo;
			$materias{$cod}{area} = $area;
			$materias{$cod}{corr} = $corr ? [ split(/\s*,\s*/, $corr) ] : [];
		}
	}
	else { # La agrego en el hash:
		$materias{$cod} = {
			nombre		=> $nombre,
			creditos	=> $cred,
			tipo		=> $tipo,
			area		=> $area,
			corr 		=> $corr ? [ split(/\s*,\s*/, $corr) ] : [],
			estado		=> 'N',
		};

		# La agrego en el cache:
		if ($fh_cache) {
			print $fh_cache ($cod, "\t", $nombre, "\t", $cred, "\n");
		}
	}
}


# leer_carrera()
#
# Parsea el archivo descriptivo de la carrera y guarda los datos en
# %carrera y %materias
sub leer_carrera
{
	if (!$opciones{cachefile}) {
		open($fh_materias, "< ".$opciones{matfile}) 
			or die $opciones{matfile}.": $!";
	} elsif (&cache_desactualizado()) {
		open($fh_materias, "< ".$opciones{matfile})
			or die $opciones{matfile}.": $!";
		print STDERR "Generando cache de materias...\n";
		open($fh_cache, "> ".$opciones{cachefile}) 
			or die $opciones{cachefile}.": $!";
	} else {
		open($fh_materias, "< ".$opciones{cachefile}) 
			or die $opciones{cachefile}.": $!";
	}
	open($fh_carrera,  "< ".$opciones{carrfile})
		or die $opciones{carrfile}.": $!";

	# Variables de estado para leer las declaraciones de las materias:
	my $tipo = "OBL";
	my $area = 0;

	while (&get_line($fh_carrera)) {
		if (/^\s*carrera\s*=\s*(\S.*)$/) { $carrera{nombre} = $1 }
		elsif (/^\s*creditos\s*=\s*(\d+)$/) { $carrera{creditos} = $1 }
		elsif (/^\s*tiene_tesis\s*=\s*(\d)$/) { $carrera{tiene_tesis} = $1 }
		elsif (/^\s*creditos_obl\s*=\s*(\d+)$/) { $carrera{creditos_obl} = $1 }
		elsif (/^\s*creditos_obl_area\s*=\s*(\d+)$/) { $carrera{creditos_obl_area} = $1 }
		elsif (/^\s*creditos_tesis\s*=\s*(\d+)$/) { $carrera{creditos_tesis} = $1 }
		elsif (/^\s*creditos_tprof\s*=\s*(\d+)$/) { $carrera{creditos_tprof} = $1 }
		elsif (/^\[obligatorias]$/) { 
			# Si son obligatorias de otro �rea las tomo como optativas:
			if (!exists $personal{area} or !$area or 
				!$personal{area} or $area eq $personal{area})
			{
					$tipo = "OBL"; 
			}
			else { $tipo = "OPT" }
		}
		elsif (/^\[optativas]$/) { $tipo = "OPT" }
		elsif (/^\[area\s+(\w+)\s+([^\]]+)]$/) { 
			$carrera{areas}{$1} = $2;
			$area = $1;
		}
		elsif (/^(\d\d\.\d\d)\s*(.*)$/) { &agregar_materia($1, $2, $tipo, $area) }
		else { die "Error de sintaxis en ".$opciones{carrfile} }
	}

	close($fh_materias) or die("$!");
	(close($fh_cache) or die("$!")) if $fh_cache;
	undef $fh_cache;
	close($fh_carrera)  or die("$!");
}

# leer_datos()
#
# Parsea los archivos de informaci�n personal y el de la carrera y guarda los
# datos en %personal y %carrera
sub leer_datos
{
	my $sum_notas = 0; 					# para calcular el promedio

	open($fh_personal, "< ".$opciones{persfile})
		or die $opciones{persfile}.": $!";

	# Busco la l�nea que indica el archivo de descripci�n de la carrera:
	while (&get_line($fh_personal)) {
		if (/^\s*carrera\s*=\s*(.+)$/) { $opciones{carrfile} = $1; last; }
	}
	# Vuelvo al principio:
	seek $fh_personal, 0, 0    or die("$!");
	# Busco la l�nea que indica el �rea cursada:
	while (&get_line($fh_personal)) {
		if (/^\s*area\s*=\s*(\w+)$/) { $personal{area} = $1; last; }
	}
	# Vuelvo al principio:
	seek $fh_personal, 0, 0    or die("$!");

	&leer_carrera();

	# Variables de estado para leer el archivo personal:
	my $estado = "A";

	# Leo el resto del archivo personal:
	while (&get_line($fh_personal)) {
		if (m!^\s*inicio\s*=\s*(\d+)/(\d+)/(\d+)!) {
			$personal{fechain} = timegm(0,0,0,$1,$2 - 1,$3);
		}
		elsif (/^\s*tesis\s*=\s*(\d+)$/) { $personal{tesis} = $1 }
		elsif (/^\[aprobadas]$/) { $estado = 'A' }
		elsif (/^\[cursando]$/) { $estado = 'C' }
		elsif (/^(\d\d\.\d\d)\s*(\d*)$/) {
			if (!exists $materias{$1})
				{ die $opciones{persfile}.": No existe la materia $1" }
			$materias{$1}{estado} = $estado;
			if ($estado eq 'C') {
				$personal{mat_cur}++;
				$personal{cred_cur} += $materias{$1}{creditos};
			}
			elsif ($estado eq 'A') {
				$materias{$1}{nota} = $2;
				$personal{mat_apr}++;
				$personal{cred_apr} += $materias{$1}{creditos};
				$personal{cred_area} += $materias{$1}{creditos} 
					if ($personal{area} and 
						$materias{$1}{area} eq $personal{area});
				$sum_notas += $materias{$1}{nota};
			}
		}
		# Ignorar las l�neas que ya fueron tratadas:
		elsif (/^\s*carrera\s*=\s*.+$/) { next }
		elsif (/^\s*area\s*=\s*\w+$/) { next }
		# Error:
		else { die $opciones{persfile}.": Error de sintaxis" }
	}
	close($fh_personal) or die("$!");

	# Calculo el promedio:
	$personal{promedio} = $sum_notas / $personal{mat_apr}
		if $personal{mat_apr} > 0;
}


# aprobado("cod")
#
# "cod" puede ser el c�digo de una materia o un requisito. La funci�n devuelve
# 1 si el requisito est� satisfecho.
sub aprobado
{
	my $req = shift;

	for ($req) {
		if (/^(\d\d\.\d\d)/) { # materia requerida
			return 0 unless $materias{$1}{estado} eq 'A';
		} elsif (/^!(\d\d\.\d\d)/) { # materia incompatible
			return 0 if $materias{$1}{estado} eq 'A';
		} elsif (/^(\d+)m/) { # cantidad de materias requerida
			return 0 if $personal{mat_apr} < $1;
		} elsif (/^(\d+)c/) { # cantidad de cr�ditos requerida
			return 0 if $personal{cred_apr} < $1;
		} else { die "Requisito inv�lido: $_" }
	}

	return 1;
}


# disponible("cod")
#
# Devuelve 1 si las correlativas est�n aprobadas.
sub disponible
{
	my ($cod) = @_;

	foreach (@{$materias{$cod}{corr}}) { # por cada correlativa:
		return 0 if (!&aprobado($_))
	}

	return 1;
}

# determinar_disponibles()
#
# Seg�n las materias aprobadas determina cu�les est�n disponibles para cursar.
sub determinar_disponibles
{
	foreach my $cod (keys %materias) { # por cada materia:
		for ($materias{$cod}{estado}) {
			if (/^A$/ and !&disponible($cod)) {
				warn "�C�mo hiciste para aprobar $cod sin aprobar las correlativas?\n"
			} elsif (/^C$/ and !&disponible($cod)) { 
				warn "�C�mo hac�s para cursar $cod si no aprobaste las correlativas?\n"
			} elsif (/^N|D$/ and &disponible($cod)) {
				$materias{$cod}{estado} = 'D';
			}
		}
	}
}


# consulta(\%filtro)
#
# Devuelve una lista ordenada con los c�digos de las materias que concuerdan
# con el filtro.
# Los elementos utilizados del hash %filtro son:
#	estado		=> 0, 'A', 'N', etc...
#	tipo		=> 0, 'OBL', 'OPT'
#	area		=> 0, 'SIDI', 'SIPR', etc...
#	creditos	=> 0, 6, 8, etc...
#	regexp		=> "", "matem", "^s", "66", etc...
# En todos los casos, 0 o "" significa no filtrar.
sub consulta
{
	my $filtro = shift;
	my @cods = ();

	foreach my $cod (keys %materias) {
		if (exists $filtro->{estado} and $filtro->{estado}) {
			next if $materias{$cod}{estado} ne $filtro->{estado};
		}
		if (exists $filtro->{tipo} and $filtro->{tipo}) {
			next if $materias{$cod}{tipo} ne $filtro->{tipo};
		}
		if (exists $filtro->{area} and $filtro->{area}) {
			next if $materias{$cod}{area} ne $filtro->{area};
		}
		if (exists $filtro->{creditos} and $filtro->{creditos}) {
			next if $materias{$cod}{creditos} != $filtro->{creditos};
		}
		if (exists $filtro->{regexp} and $filtro->{regexp}) {
			next if (
				$materias{$cod}{nombre} !~ /$filtro->{regexp}/i
				and
				$cod !~ /$filtro->{regexp}/i
			);
		}
		push @cods, $cod;
	}

	return sort @cods;
}


# correlativas({cod => "COD", full => 1, nivel => 0, nivmax => 0})
#
# Devuelve el �rbol de correlatividad para COD. Si full == 0 solo imprime los
# requisitos que no est�n satisfechos. Si $nivmax > 0, determina el nivel
# m�ximo de recursividad. El hash devuelto es algo como:
# $corr = listar_correlativas("66.33", 1, 0);
# $corr es: {
#   25.49 => {
#     75.04 => {},
#     66.07 => {
#     	25.90 => {},
#     	20m => {},
#     },
#   },
#   33.66 => {},
# }
sub correlativas
{
	my $parms = shift;

	my $cod    = exists $parms->{cod}    ? $parms->{cod}    : 0;
	my $full   = exists $parms->{full}   ? $parms->{full}   : 1;
	my $nivel  = exists $parms->{nivel}  ? $parms->{nivel}  : 0;
	my $nivmax = exists $parms->{nivmax} ? $parms->{nivmax} : 0;

	die "No existe la materia $cod" unless exists $materias{$cod};

	my $arbol = { };

	if ($nivmax and $nivel == $nivmax) { return $arbol }
	$nivel++;

	foreach my $corr (@{$materias{$cod}{corr}}) { # por cada correlativa
		if ($full or !&aprobado($corr)) { 
			if ($corr =~ /^\d\d\.\d\d/) {
				# La correlativa es una materia
				$arbol->{$corr} = &correlativas(
					{ cod => $corr, full => $full, 
					nivel => $nivel, nivmax =>$nivmax }
				);
			} else {
				# no es una materia:
				$arbol->{$corr} = {};
			}
		}
	}

	return $arbol;
}


# habilita("COD")
#
# Devuelve una lista con los c�digos de las materias que dependen de $cod,
# o lo que es lo mismo, las materias que habilita $cod.
sub habilita
{
	my ($cod) = @_;
	my @cods = ();

	die "No existe la materia $cod" unless exists $materias{$cod};

	CORR: foreach my $cod2 (keys %materias) { 	# por cada materia $cod2
		foreach (@{$materias{$cod2}{corr}}) {   # por cada correlativa de $cod2
			if ($cod eq $_) {					# si $cod es correlat. de $cod2
				# agrego $cod2 a la lista:
				push @cods, $cod2;
				next CORR;
			}
		}
	}

	return @cods;
}


# guardar_datos_personales()
#
# Guarda los datos personales en personal.dat.
sub guardar_datos_personales
{
	open (my $fh, "> ".$opciones{persfile}) or die $opciones{persfile}.": $!";

	print $fh "# personal.dat generado autom�ticamente por ".$APPNAME."\n";
	print $fh "carrera = ".$opciones{carrfile}."\n";

	if (exists $personal{fechain}) {
		my @inicio = gmtime($personal{fechain});
		printf $fh "inicio = %.2d/%.2d/%d\n",
			$inicio[3], $inicio[4] + 1, $inicio[5] + 1900;
	}
	
	print $fh ("tesis = ".$personal{tesis}."\n") if exists $personal{tesis};
	print $fh ("area = ".$personal{area}."\n") if exists $personal{area};

	print $fh ("\n[aprobadas]\n");
	for (keys %materias) {
		print $fh ($_." ".$materias{$_}{nota}."\n") 
			if $materias{$_}{estado} eq 'A';
	}
	
	print $fh ("\n[cursando]\n");
	for (keys %materias) {
		print $fh ($_."\n")
			if $materias{$_}{estado} eq 'C';
	}
	
	close ($fh) or die $opciones{persfile}.": $!";
}


# cambiar_estado_materia($cod, $estado[, $nota])
#
# Cambia el estado de la materia en %materias, y vuelve a calcular las materias
# disponibles.
sub cambiar_estado_materia
{
	my ($cod, $estado, $nota) = @_;

	die "No existe la materia $cod" if not exists $materias{$cod};

	$materias{$cod}{estado} = $estado;
	$materias{$cod}{nota} = $nota if $estado eq 'A';
	&determinar_disponibles();
	&guardar_datos_personales();
}


# init()
#
# Inicializa los hashes con los datos leidos de los archivos.
sub init
{
	local $_; # necesario porque $_ viene ligado a $opciones{accion} (?)

	&leer_datos();
	&determinar_disponibles();
}


# reinit()
#
# Vuelve a leer los datos de los archivos y los guarda en los hashes.
sub reinit
{
	local $_; # necesario porque $_ viene ligado a $opciones{accion} (?)

	&init_hashes();
	&leer_datos();
	&determinar_disponibles();
}

#==============================================================================
&init_hashes();
1;

