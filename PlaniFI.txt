
Utilizaci�n del m�dulo PlaniFI.pm
---------------------------------

INICIALIZACION

    use PlaniFI;

    &PlaniFI::init();   # Lee los archivos .dat y vuelca los datos obtenidos
                        # en los hashes.

  Es posible cambiar la ubicaci�n de los archivos .dat modificando las 
  variables que se listan a continuaci�n, antes de llamar a init(). Los 
  valores mostrados son los utilizados en forma predeterminada:

    $PlaniFI::opciones{persfile}  = "personal.dat";
    $PlaniFI::opciones{matfile}   = "materias.dat";
    $PlaniFI::opciones{cachefile} = ".planifi.cache";

  Se puede prevenir la utilizaci�n del cache de materias (es decir, que
  se lean las materias directamente de materias.dat, sin intentar utilizar
  el cache), asignando:
  
    $PlaniFI::opciones{cachefile} = "";

  Adicionalmente, las siguientes variables est�n disponibles:

    $PlaniFI::APPNAME   # "PlaniFI"
    $PlaniFI::VERSION   # "1.1"


HASHES

  Luego de ejecutar init(), los siguientes hashes est�n disponibles:

    %personal    datos personales, como la cantidad de cr�ditos acumulados
                 y el promedio.

    %carrera     datos relativos a la carrera (el nobre, las �reas, etc).

    %materias    todas las materias y sus propiedades.


Claves de %personal

  Las siguientes claves est�n siempre disponibles en el hash %personal:

    cred_apr    => creditos acumulados
    cred_cur    => creditos cursando
    cred_area   => creditos acumulados en el area
    mat_apr     => cantidad de materias aprobadas
    mat_cur     => cantidad de materias cursando
    promedio    => promedio

  Las claves que siguen son creadas �nicamente si el usuario ingresa los datos
  correspondientes en personal.dat:

    area        => �rea elegida.
    fechain     => fecha de inicio (cantidad de segundos desde 1970).
    tesis       => 1 si se elige hacer tesis.


Claves de %carrera

  Las siguientes claves est�n siempre disponibles en el hash %carrera:

    tipos       => { OBL => "obligatoria", OPT => "optativa" }
    estados     => {
                     N => "no disponible para cursar",
                     D => "disponible para cursar",
                     A => "aprobada",
                     C => "cursando",
                   }
    areas       => Si hay areas definidas en el archivo de la carrera, se 
                   ingresan en este hash. Por ejemplo:
                   {
                     COMP => "Computadoras",
                     COMU => "Comunicaciones",
                   }

  Las claves que siguen son creadas �nicamente si los datos correspondientes
  son encontrados en los archivos .dat:

    nombre            => nombre de la carrera
    creditos          => cantidad total de creditos para recibirse
    creditos_obl      => cantidad de creditos en materias obligatorias
    creditos_obl_area => cantidad de creditos en materias obligatorias del
                         area elegida
    creditos_tesis    => cantidad de creditos en materias optativas del area
                         elegida necesarios para realizar la tesis
    creditos_tprof    => idem para el trabajo profesional
    tiene_tesis       => 0 si la carrera no requiere Tesis ni Trabajo Prof.


El hash %materias

  Las claves de %materias son c�digos de materias, y las propiedades de cada
  materia se guardan en un hash. Por ejemplo, $materias{"61.08"}{nombre} es
  "Algebra II".

  Para cada materia, las siguientes claves est�n disponibles:

    nombre      => nombre de la materia
    creditos    => cantidad de cr�ditos
    tipo        => "OBL" o "OPT"
    area        => el c�digo de un �rea, o 0 si no es de ning�n �rea.
    corr        => Una lista de c�digos de correlativas. Ver m�s adelante.
    estado      => 'A', 'N', etc (Ver %carrera{estados})
    nota        => Nota de la materia. (s�lo disponible si el estado es 'A')

  Las correlatividades se guardan en una lista. La misma puede contener 4
  tipos de valores:

    "66.06"     Una materia
    "!66.06"    Una materia que NO debe estar aprobada
    "140c"      cantidad de cr�ditos
    "20m"       cantidad de materias

  Por ejemplo, las correlativas de "75.04" pueden ser algo como:

    $materias{"75.04"}{corr} = [ "75.01", "66.06", "!63.10", "120c" ];


FUNCIONES

  Adem�s de init(), PlaniFI.pm provee las siguientes funciones:


consulta(\%filtro)

  Devuelve una lista ordenada con los c�digos de las materias que concuerdan
  con el filtro.

  Los elementos utilizados del hash %filtro son:
    estado     => 0, 'A', 'N', etc...
    tipo       => 0, 'OBL', 'OPT'
    area       => 0, 'SIDI', 'SIPR', etc...
    creditos   => 0, 6, 8, etc...
    regexp     => "", "matem", "^s", etc...

  En todos los casos, el valor por defecto es 0 o "", que significa no 
  filtrar.

  Ejemplos de uso:

    @codigos = &consulta( { } )   # todas las materias.

    @codigos = &consulta( { estado => 'A' } )   # Materias aprobadas
    
    @codigos = &consulta( { tipo => 'OPT', area => 'SIDI' } ) 
       # Materias optativas de Sistemas Distribuidos.


correlativas({cod => "COD", full => 1, nivmax => 0})

  Devuelve una referencia a un hash que contiene el �rbol de correlatividad
  para COD. 

  Si full == 0 solo imprime los requisitos que no est�n satisfechos.
  Si es especificado, nivmax determina el nivel m�ximo de recursividad. 

  Ejemplo de uso:

    $corr = &correlativas( { cod => "66.33" } );
 
  Suponiendo que las correlativas de 66.33 son 25.49 y 33.66, $corr puede
  quedar conteniendo algo como: 
    {
      25.49 => {
                 75.04 => {},
                 66.07 => {
                            25.90 => {},
                            20m => {},
                          },
               },
      33.66 => {},
    }


habilita("66.33")

  Devuelve una lista con los c�digos de las materias que dependen de "66.33",
  o lo que es lo mismo, las materias que habilita "66.33".


carreras_disponibles()

  Devuelve una referencia a un hash con informaci�n acerca de las carreras
  disponibles. El hash devuelto tiene la siguiente estructura:
   {
     informatica.dat => {
  			 nombre => "Ing. en Inform�tica",
  			 areas  => { 
  				    SIDI => "Sistemas Distribuidos",
  				    SIPR => "Sistemas de Producci�n", 
  				   },
  			 tiene_tesis => 1,
  			},
     agrimensura.dat => {
  			 nombre => "Agrimensura",
 			 areas => {},
 			 tiene_tesis => 0,
  			},
   }
  No es necesario llamar a init() antes de utilizar esta funci�n.


cambiar_estado_materia($cod, $estado[, $nota])

  Cambia el estado de la materia en %materias, y vuelve a calcular las materias
  disponibles. Modifica el archivo personal.dat.

  Ejemplos de uso:
  	cambiar_estado_materia('61.08', 'C');
  	cambiar_estado_materia('61.09', 'N');
  	cambiar_estado_materia('61.10', 'A', 8);


guardar_datos_personales()

  Genera el archivo personal.dat, seg�n los datos cargados en los hashes.


PlaniFI::reinit()

  Vuelve a leer todos los datos. Es �til para cuando hay una nueva materia
  aprobada y se desea recalcular las estad�sticas.


